# ref: https://pytorch.org/tutorials/beginner/introyt/trainingyt.html
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torchinfo import summary

    
device = 'cuda' if torch.cuda.is_available() else 'cpu'


class SimpleCNN(nn.Module):
    def __init__(self, in_channels=1, num_classes=10):
        super(SimpleCNN, self).__init__()
        self.conv1 = nn.Conv2d(
            in_channels=1,
            out_channels=8,
            kernel_size=(3,3),
            stride=(1,1),
            padding=(1,1),
        )
        self.pool = nn.MaxPool2d(
            kernel_size=(2,2),
            stride=(2,2),
        )
        self.conv2 = nn.Conv2d(
            in_channels=8,
            out_channels=16,
            kernel_size=(3,3),
            stride=(1,1),
            padding=(1,1),
        )
        self.fc1 = nn.Linear(
            16*7*7, #after 2 pools, 28-->7
            num_classes,
        )

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = x.flatten(start_dim=1)
        x = self.fc1(x)
        return x


def simple_test_NN():
    model = SimpleCNN(784, 10)
    x = torch.randn(64, 784)
    print(model(x).shape)


def load_data(batch_size=32):
    ds_path = '../datasets/'
    train_ds = datasets.MNIST(
        root=ds_path,
        train=True,
        transform=transforms.ToTensor(),
        download=True,
    )
    train_loader = DataLoader(
        dataset=train_ds,
        batch_size=batch_size,
        shuffle=True,
    )
    test_ds = datasets.MNIST(
        root=ds_path,
        train=False,
        transform=transforms.ToTensor(),
        download=True,
    )
    test_loader = DataLoader(
        dataset=test_ds,
        batch_size=batch_size,
        shuffle=True,
    )
    return train_loader, test_loader


def train(model, num_epochs, optimizer, loss_fn, train_loader, test_loader):
    for epoch in range(num_epochs):
        print(f'epoch: {epoch+1}/{num_epochs}')
        for batch_idx, (data, targets) in enumerate(train_loader):
            data = data.to(device)
            targets = targets.to(device)
    
            # zero the gradients for every batch
            optimizer.zero_grad()

            # forward: make predictions
            outputs = model(data)
    
            # backward: compute the loss and its gradients
            loss = loss_fn(outputs, targets)
            loss.backward()
    
            # gradient descent or adam step
            optimizer.step()
        check_accuracy(train_loader, model)
        check_accuracy(test_loader, model)


def check_accuracy(loader, model):
    if loader.dataset.train:
        print('train ', end='')
    else:
        print('test ', end='')

    num_correct = 0
    num_samples = 0

    #https://stackoverflow.com/a/60018731/9721896
    #https://stackoverflow.com/a/66843176/9721896
    #model.eval()
    with torch.no_grad():
        for x, y in loader:
            x = x.to(device)
            y = y.to(device)

            outputs = model(x) #(batch_size, num_classes)
            # Because the output classes are from 0 to 9
            # which match the indices, we use the max index
            # (pred) to represent the class label.
            _, pred = outputs.max(dim=1)
            num_correct += (pred == y).sum()
            num_samples += pred.size(dim=0)
        print(f'acc: {num_correct/num_samples*100:.2f}%')


def main():
    in_channels = 1
    num_classes = 10
    learning_rate = 0.001
    batch_size = 64
    num_epochs = 3

    train_loader, test_loader = load_data(batch_size)

    model = SimpleCNN(
        in_channels=in_channels, num_classes=num_classes
    ).to(device)
    #summary(model, input_size=(batch_size, 1, 28, 28), col_names=['input_size'])
    
    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    train(model, num_epochs, optimizer, loss_fn, train_loader, test_loader)


if __name__=='__main__':
    main()
