import torch

#-- view vs. reshape
x = torch.arange(12)
x_43 = x.view(4, 3) #contiguous
print(x_43)

x_t = x_43.t() #non-contiguous
print(x_t)

#x_t_ = x_t.view(12) #won't work
#x_t_ = x_t.reshape(12) #works
x_t_ = x_t.contiguous().view(12)
print(x_t_)

#-- concat tensors
x1 = torch.rand(2,3)
x2 = torch.rand(2,3)
print(torch.cat((x1,x2)).shape)
print(torch.cat((x1,x2), dim=0).shape)
print(torch.cat((x1,x2), dim=1).shape)

#-- flatten
z = x1.view(-1)
print('flatten all: ', x1.shape, z.shape)

batch = 32
x = torch.rand(batch, 2, 5)
z = x.view(batch, -1)
print('flatten along some dimensions: ', x.shape, z.shape)

z = x.permute(0, 2, 1)
print('exchange some dimensions: ', x.shape, z.shape)

#-- add dimension
x = torch.rand(5)
print(x.unsqueeze(0).shape)
print(x.unsqueeze(1).shape)

#-- remove dimension
x = torch.rand(5,2,1,3)
print(x.shape)
z = x.squeeze() #auto remove the dimension of size 1
#z = x.squeeze(2) #the same as the above one
print(z.shape)


