import torch

batch_size = 10
features = 25

x = torch.rand(batch_size, features)

print(x[0,:].shape)
print(x[:,0].shape)
x[2,0] = 100
print(x[2,0:10])

x = torch.arange(10)
idx = [2,8,5]
print(x[idx])

x = torch.arange(12).reshape(4,3)
print(x)
print(x.ndimension(), x.dim())
print(x.numel())

rows = torch.tensor([2,3])
cols = torch.tensor([0,2])
print(f'x[{rows[0]},{cols[0]}] '
      f'and x[{rows[1]},{cols[1]}] are '
      f'{x[rows, cols]}')

print(x[(x<2) | (x>8)])
print(x[(x>2) & (x<8)])
print(x[x.remainder(2) == 0])
print(x[x%2 == 0])
print(torch.where(x>5, x, -x))
print(torch.tensor([0,0,1,2,2,3,4]).unique())

