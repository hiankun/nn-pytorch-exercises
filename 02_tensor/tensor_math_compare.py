import torch

#-- math
x = torch.tensor([1,2,3])
y = torch.tensor([7,8,9])
print(
    f'x + y = {x+y}\n',
    f'x - y = {x-y}\n',
    f'x / y = {torch.true_divide(x,y)}\n',
    f'x ** 2 = {x.pow(2)}\n',
    f'x ** 2 = {x**2}\n',
)

# comparison
print(x <= 1)
print(x > 1)

#-- matrix multiplication (mm)
x1 = torch.rand(2,5)
x2 = torch.rand(5,3)
x3 = torch.mm(x1, x2)
x3 = x1.mm(x2)
print(f'torch.mm(x1, x) = {x3}')

#-- element-wise multiplication
x1 = torch.rand(2,5)
x2 = torch.rand(2,5)
x3 = x1 * x2
print(f'x1 * x2 = {x3}')
#-- dot product
z = torch.dot(x, y)
print(f'torch.dot(x, y) = {z}')

#-- matrix exp
_x = torch.arange(4).reshape(2,2)
x_2 = _x.matrix_power(2)
print(x_2)
x_2 = _x**2 #note: element-wise
print(x_2)

#-- inplace operations
t = torch.zeros(3)
t.add_(x) #inplace
t += x #inplace
t = t + x #copy

#-- batch matrix multiplication
batch = 8
n, m, p = 3, 4, 5
tsr1 = torch.rand(batch, n, m)
tsr2 = torch.rand(batch, m, p)
out_bmm = torch.bmm(tsr1, tsr2)
print(f'tsr1.shape = {tsr1.shape}')
print(f'tsr2.shape = {tsr2.shape}')
print(f'out_bmm.shape = {out_bmm.shape}')

#-- broadcasting
x1 = torch.arange(5,11).reshape(2,3)
x2 = torch.arange(1,4)
print(f'x1 = {x1}\nx2 = {x2}')
z = x1 - x2
print(f'x1 - x2 = {z}')
z = x1 * x2
print(f'x1 * x2 = {z}')
z = x1 / x2
print(f'x1 / x2 = {z}')
z = x1 ** x2
print(f'x1 ** x2 = {z}')

#-- misc
z = torch.sum(x1)
print(f'sum(x1) = {z}')
z = torch.sum(x1, dim=0)
print(f'sum(x1, dim=0) = {z}')
z = torch.sum(x1, dim=1)
print(f'sum(x1, dim=1) = {z}')

val, idx = torch.max(x1, dim=0)
print(f'max(x1, dim=0) = {val} at {idx}')
val, idx = torch.max(x1, dim=1)
print(f'max(x1, dim=1) = {val} at {idx}')
idx = torch.argmax(x1, dim=0)
print(f'argmax(x1, dim=0) = {idx}')
idx = torch.argmax(x1, dim=1)
print(f'argmax(x1, dim=1) = {idx}')

z = torch.abs(x1)
print(f'abs(x1) = {z}')
z = torch.mean(x1.float()) #note: only float/complex
print(f'mean(x1) = {z}')
z = torch.mean(x1.float(), dim=0)
print(f'mean(x1, dim=0) = {z}')
z = torch.mean(x1.float(), dim=1)
print(f'mean(x1, dim=1) = {z}')

z = torch.eq(x1, x2)
print(f'eq(x1, x2) = {z}')

sorted_x1, _idx = torch.sort(x1, descending=True)
print(sorted_x1, _idx)

z = torch.clamp(x1, min=6, max=9)
print(f'clamped x1 = {z}')

x_b = torch.tensor([1,0,1,1]).bool()
print(f'any element in x_b is True: {torch.any(x_b)}')
print(f'all elements in x_b are True: {torch.all(x_b)}')
