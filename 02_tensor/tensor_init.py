import torch

#-- initializing tensors

device = 'cuda' if torch.cuda.is_available() else 'cpu'

my_tensor = torch.tensor(
    [[1, 2, 3], [4, 5, 6]],
    dtype=torch.float32,
    device=device,
    requires_grad=True,
)

print(
    my_tensor,
    my_tensor.dtype,
    my_tensor.shape,
    my_tensor.device,
    my_tensor.requires_grad,
)

#-- other methods

x = torch.empty(3,3)
x = torch.zeros(3,3)
x = torch.ones(3,3)
x = torch.eye(3)
x = torch.diag(torch.ones(3))
x = torch.rand(3,3)

x = torch.arange(0,5,1) #start, end, step
x = torch.linspace(0,5,11) #start, end, steps <--note

x = torch.empty(1,5).normal_(mean=0, std=1)
x = torch.empty(1,5).uniform_(0, to=1)
# The `from` clashes Python keywords and won't work.
#x = torch.empty((1,5)).uniform_(from=0, to=1)

#-- convert to other types
x = torch.arange(3)
print(
    f'bool: {x.bool()}, {x.bool().dtype}\n',
    f'short: {x.short()}, {x.short().dtype}\n',
    f'long: {x.long()}, {x.long().dtype}\n',
    f'half: {x.half()}, {x.half().dtype}\n',
    f'float: {x.float()}, {x.float().dtype}\n',
    f'double: {x.double()}, {x.double().dtype}\n',
)

#-- from/to numpy
import numpy as np
np_arr = np.zeros((3,3))
pt_tsr = torch.from_numpy(np_arr)
np_arr_back = pt_tsr.numpy()
print(
    f'np arr: {np_arr}, {np_arr.dtype}\n',
    f'tensor: {pt_tsr}\n',
    f'np_arr_back: {np_arr_back}, {np_arr_back.dtype}\n',
)

