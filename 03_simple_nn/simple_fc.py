# ref: https://pytorch.org/tutorials/beginner/introyt/trainingyt.html
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
import torchvision.datasets as datasets
import torchvision.transforms as transforms

    
device = 'cuda' if torch.cuda.is_available() else 'cpu'


class SimpleNN(nn.Module):
    def __init__(self, input_shape, num_classes):
        super(SimpleNN, self).__init__()
        self.fc1 = nn.Linear(input_shape, 50)
        self.fc2 = nn.Linear(50, num_classes)

    def forward(self, x):
        x = x.flatten(start_dim=1)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x


def simple_test_NN():
    model = SimpleNN(784, 10)
    x = torch.randn(64, 784)
    print(model(x).shape)


def load_data(batch_size=32):
    ds_path = '../datasets/'
    train_ds = datasets.MNIST(
        root=ds_path,
        train=True,
        transform=transforms.ToTensor(),
        download=True,
    )
    train_loader = DataLoader(
        dataset=train_ds,
        batch_size=batch_size,
        shuffle=True,
    )
    test_ds = datasets.MNIST(
        root=ds_path,
        train=False,
        transform=transforms.ToTensor(),
        download=True,
    )
    test_loader = DataLoader(
        dataset=test_ds,
        batch_size=batch_size,
        shuffle=True,
    )
    return train_loader, test_loader


def train(model, num_epochs, optimizer, loss_fn, train_loader, test_loader):
    for epoch in range(num_epochs):
        print(f'epoch: {epoch+1}/{num_epochs}')
        for batch_idx, (data, targets) in enumerate(train_loader):
            data = data.to(device)
            targets = targets.to(device)
    
            # zero the gradients for every batch
            optimizer.zero_grad()

            # forward: make predictions
            outputs = model(data)
    
            # backward: compute the loss and its gradients
            loss = loss_fn(outputs, targets)
            loss.backward()
    
            # gradient descent or adam step
            optimizer.step()
        check_accuracy(train_loader, model)
        check_accuracy(test_loader, model)


def check_accuracy(loader, model):
    if loader.dataset.train:
        print('train ', end='')
    else:
        print('test ', end='')

    num_correct = 0
    num_samples = 0

    #https://stackoverflow.com/a/60018731/9721896
    #https://stackoverflow.com/a/66843176/9721896
    #model.eval()
    with torch.no_grad():
        for x, y in loader:
            x = x.to(device)
            y = y.to(device)

            outputs = model(x) #(batch_size, num_classes)
            # Because the output classes are from 0 to 9
            # which match the indices, we use the max index
            # (pred) to represent the class label.
            _, pred = outputs.max(dim=1)
            num_correct += (pred == y).sum()
            num_samples += pred.size(dim=0)
        print(f'acc: {num_correct/num_samples*100:.2f}%')


def main():
    input_shape = 784
    num_classes = 10
    learning_rate = 0.001
    batch_size = 64
    num_epochs = 3

    train_loader, test_loader = load_data(batch_size)

    model = SimpleNN(
        input_shape=input_shape, num_classes=num_classes
    ).to(device)
    
    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    train(model, num_epochs, optimizer, loss_fn, train_loader, test_loader)


if __name__=='__main__':
    main()
